<?php

use Slim\Http\Request;
use Slim\Http\Response;
use App\Middleware\ParametersChecker;
use DavidePastore\Slim\Validation\Validation;

// Routes
$app->group('/user', function () {
    $this->get('/new', App\Controller\UsersController::class . ':new');

    $this->delete('', App\Controller\UsersController::class . ':delete');
});

$app->group('/favorite', function () {
    $this->post('', App\Controller\FavoritesController::class . ':add')
        ->add(new ParametersChecker())
        ->add(new Validation(App\Controller\FavoritesController::addValidator()));

    $this->put('/{id}', App\Controller\FavoritesController::class . ':update')
        ->add(new ParametersChecker())
        ->add(new Validation(App\Controller\FavoritesController::updateValidator()));

    $this->get('', App\Controller\FavoritesController::class . ':get');

    $this->delete('/{id}', App\Controller\FavoritesController::class . ':delete')
        ->add(new ParametersChecker())
        ->add(new Validation(App\Controller\FavoritesController::deleteValidator()));
});