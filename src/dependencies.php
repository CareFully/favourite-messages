<?php

use Slim\Http\Response;

// DIC configuration

// Handlers
$container['errorHandler'] = function ($c) {
    return new App\Service\ApiError(
        $c->get('logger'),
        $c->get('settings')['displayErrorDetails']
    );
};

$container['notAllowedHandler'] = function ($c) {
    return function ($request, $response, $methods) use ($c) {
        $allowedMethods = implode(', ', $methods);
        $message = sprintf('Method %s not allowed. Must be one of %s', $request->getMethod(), $allowedMethods);
        $c->get('logger')->warning($message);
        return (new Response(405))
            ->withJSON(['message' => $message])
            ->withHeader('Allow', $allowedMethods);
    };
};

$container['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        return (new Response(404))->withJSON(['message' => 'Route not found']);
    };
};

$container['phpErrorHandler'] = function ($c) {
    return new App\Service\ApiError(
        $c->get('logger'),
        $c->get('settings')['displayErrorDetails']
    );
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

// Service factory for the ORM
$container['db'] = function ($c) {
    $capsule = new \Illuminate\Database\Capsule\Manager;
    $capsule->addConnection($c->get('settings')['db']);

    $capsule->setAsGlobal();
    $capsule->bootEloquent();

    return $capsule;
};

$container[App\Controller\UsersController::class] = function ($c) {
    $logger = $c->get('logger');
    $table = $c->get('db')->table('users');
    $secret = $c->get('settings')['secret'];
    if ($c->has("user")) {
        $user = $c->get("user");
    } else {
        $user = null;
    }
    return new App\Controller\UsersController($logger, $table, $secret, $user);
};

$container[App\Controller\FavoritesController::class] = function ($c) {
    $logger = $c->get('logger');
    $table = $c->get('db')->table('messages');
    $user = $c->get("user");
    return new App\Controller\FavoritesController($logger, $table, $user);
};