<?php
namespace App\Controller;

use Illuminate\Database\Query\Builder;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Respect\Validation\Validator as v;

class FavoritesController 
{
    protected $logger;
    protected $table;
    protected $user;

    public function __construct(\Monolog\Logger $logger, Builder $table, $user)
    {
        $this->logger = $logger;
        $this->table = $table;
        $this->user = $user;
    }

    public function add(Request $request, Response $response, array $args) 
    {  
        $body = $request->getParsedBody();
        $data = [];

        if(isset($body['description'])) {
            $body['description'] = trim($body['description']);
        } else {
            $body['description'] = null;
        }

        $this->table->insert([
            'user_id' => $this->user->id,
            'name' => trim($body['name']),
            'message' => trim($body['message']),
            'description' => $body['description']
        ]);

        return $response->withJson(
            ['message' => 'Message added'],
            201
        );
    }

    public static function addValidator()
    {
        $nameValidator = v::stringType()->length(1, 64);
        $messageValidator = v::stringType()->notEmpty();
        $descriptionValidator = v::optional(v::stringType());
        return [
            'name' => $nameValidator,
            'message' => $messageValidator,
            'description' => $descriptionValidator
        ];
    }

    public function get(Request $request, Response $response, array $args) 
    {
        
        $favorites = $this->table
            ->where('user_id', '=', $this->user->id)
            ->get(['id', 'name', 'message', 'description', 'created']);

        return $response->withJson($favorites);
    }

    public function update(Request $request, Response $response, array $args) 
    {  
        $body = $request->getParsedBody();
        $data = [];

        $favorite = $this->table->where(['id' => $args['id'], 'user_id' => $this->user->id])->first(['id']);

        if ($favorite === null) {
            $data['message'] = 'Favorite message not found or does not belong to current user';
            $response = $response->withStatus(404);
        } else {

            $columns = [];

            if (isset($body['name']) && !empty($body['name'])) {
                $columns['name'] = trim($body['name']);
            }

            if (isset($body['message']) && !empty($body['message'])) {
                $columns['message'] = trim($body['message']);
            }

            if (isset($body['description'])) {
                $columns['description'] = trim($body['description']);
            }
            
            if (count($columns) > 0) {
                $this->table->where(['id' => $args['id'], 'user_id' => $this->user->id])->update($columns);
            }
            $data['message'] = 'Message updated';
        }

        return $response->withJson($data);
    }

    public static function updateValidator()
    {
        $idValidator = v::numeric()->positive();
        $nameValidator = v::optional(v::stringType()->length(1, 64));
        $messageValidator = v::optional(v::stringType()->notEmpty());
        $descriptionValidator = v::optional(v::stringType());
        return [
            'id' => $idValidator,
            'name' => $nameValidator,
            'message' => $messageValidator,
            'description' => $descriptionValidator
        ];
    }

    public function delete(Request $request, Response $response, array $args) 
    {
        $favorites = $this->table
            ->where(['id' => $args['id'], 'user_id' => $this->user->id])
            ->delete();

        return $response->withJson([
            'message' => 'Deleted if existed'
        ]);
    }

    public static function deleteValidator()
    {
        $idValidator = v::numeric()->positive();
        return [
            'id' => $idValidator
        ];
    }
} 