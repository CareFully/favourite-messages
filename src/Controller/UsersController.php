<?php
namespace App\Controller;

use Firebase\JWT\JWT;
use Illuminate\Database\Query\Builder;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class UsersController 
{

    protected $logger;
    protected $table;
    protected $secret;
    protected $user;

    public function __construct(\Monolog\Logger $logger, Builder $table, string $secret, $user)
    {
        $this->logger = $logger;
        $this->table = $table;
        $this->secret = $secret;
        $this->user = $user;
    }

    public function new(Request $request, Response $response, array $args) 
    {
        //$this->logger->info("Slim-Skeleton '/' route");
        $now = new \DateTime();
        $jti = $this->table->insertGetId(['active' => 1]);

        $payload = [
            "jti" => $jti,
            "iat" => $now->getTimeStamp()
        ];

        return $response->withJson(
            [
                'message' => 'Account created',
                'token' => JWT::encode($payload, $this->secret, "HS256")
            ],
            201
        );
    }

    public function delete(Request $request, Response $response, array $args) 
    {
        $this->table->where('id', '=', $this->user->id)->delete();

        return $response->withJson([
            'message' => 'Account deleted'
        ]);
    }
} 