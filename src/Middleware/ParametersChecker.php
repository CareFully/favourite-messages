<?php
namespace App\Middleware;


class ParametersChecker
{
    public function __invoke($request, $response, $next)
    {
        if ($request->getAttribute('has_errors')) {
            $data['message'] = "Incorrect values";
            $data['errors'] = $request->getAttribute('errors');
            $response = $response->withStatus(422);
            return $response->withJSON($data);
        }
        return $next($request, $response);
    }
}