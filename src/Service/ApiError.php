<?php
namespace App\Service;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

final class ApiError extends \Slim\Handlers\AbstractError
{

    protected $logger;

    public function __construct(\Monolog\Logger $logger, $displayErrorDetails = false)
    {
        $this->logger = $logger;
        $this->displayErrorDetails = (bool) $displayErrorDetails;
    }


    function __invoke(Request $request, Response $response, \Throwable $exception)
    {
        $status = 500;
        $this->writeToErrorLog($exception);

        $message = $exception->getMessage();
        do {
            $exceptionData[] = [
                'type' => get_class($exception),
                'code' => $exception->getCode(),
                'message' => $exception->getMessage(),
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
                'trace' => explode("\n", $exception->getTraceAsString()),
            ];
        } while ($exception = $exception->getPrevious());

        $this->logger->critical($message, ['extra_data' => json_encode($exceptionData)]);

        if ($this->displayErrorDetails) {
            $data['message'] = $message;
            $data['exception'] = $exceptionData;
        } else {
            $data['message'] = 'Slim Application Error';
        }

        return $response->withJSON($data, $status);
    }
}