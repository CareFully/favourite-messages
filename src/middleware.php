<?php
// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);

$app->add(
    new \Slim\Middleware\JwtAuthentication([
    "path" => ["/user", "/favorite"],
    "passthrough" => ["/user/new"],
    "secret" => $container->get('settings')['secret'],
    "callback" => function ($request, $response, $arguments) use ($container) {
        $decoded = $arguments["decoded"];
        $db = $container->get('db');
        $table = $db->table('users');

        $user = $table->find($decoded->jti);

        if ($user !== null && $user->active === 1) {
            $container["user"] = $user;
        } else {
            return false;
        }
    },
    "error" => function ($request, $response, $arguments) use ($container) {
        $showDetails = $container->get('settings')['displayErrorDetails'];
        $data["message"] = ($showDetails ? $arguments["message"] : "Unauthorized");
        return $response
            ->withHeader('WWW-Authenticate', 'Bearer realm="Favorite Message User Authentication"')
            ->withJSON($data, 401);
    }
]));

