## Summary

This API Can store links and sentences that are accessable with a user token. 
Token is only given once for a single user.

## Table of contents

[TOC]

## Installation

**Summary of set up**

The following instructions will show how to setup a new instance of this API.

**Configuration**

The following environment variables are used for database connection and Json Web Token secret:
```json
"JWT_SECRET":   JSON Web Token secret
"DB_HOST":      Database hostname
"DB_NAME":      Database name
"DB_USER":      Database username
"DB_PASSWORD":  Database password
```

The names of the environment variables, as well as other settings, can be changed in `src/settings.php`.

**Database configuration**

To create required tables to the database. Run the queries that are in `database.sql`.

**How to run tests**

There are no tests to run :(

**Deployment**

Make sure your web server will use `public/index.php` as root.

## Authorization
Every endpoint (except user creation endpoint) requires authorization.
Authorization token MUST be set to "Authorization" header in the following format: Bearer <token>
Token for a new user can be acquired by requesting `GET /user/new`

The following response will be returned when authorization fails:  
HTTP 401 Unauthorized
```json
{
    "status": 401,
    "message": "Token not found" // Reason for unauthorizing.
}
```

## Errors
All endpoints will return the following response body when there has been an error:
```json
{
	"message": "<Error description>"
}
```

**Incorrect parameters**

All endpoints will return the following response when parameters are invalid: 

`HTTP 422 Unprocessable Entity`
```json
{
    "message": "Incorrect values",
    "errors": {
        "<invalid_parameter1>": [
            "<rule1>",
            "<rule2>", ...
        ],
        "message": [
            "null must be a string",
            "null must not be empty"
        ], ...
    }
}
```

## User endpoints

**GET /user/new**

Returns a token for a new user.
This endpoint requires no authentication.

Successful Response: `HTTP 201 Created`
```json
{
    "message": "Account created",
    "token": "aaaa.bbbb.cccc"
}
```

***

**DELETE /user**

Deletes current user and all associated favorite strings.
After requesting this endpoint, the current authorization token will no longer be valid.

Successful Response: `HTTP 200 OK`
```json
{
    "message": "Account deleted"
}
```

***

## Favorite endpoints

**GET /favorite**

Returns an array of favorited strings.
Array will be empty if no strings are added to favorites.

Successful Response: `HTTP 200 OK`
```json
[
    {
        "id": 4,
        "name": "FooBar",
        "message": "https://www.foo.com/",
        "description": "FooBar best Bar", // Optional, null if not specified
        "created": "2018-02-15 14:25:15"
    }, ...
]
```

***

**POST /favorite**

Store a new favorite message / url.

Parameters:
```json
Required:
    name: "FooBar"                  | Message title
    message: "https://www.foo.com/" | Favorite message

Optional:
    description: "FooBar best Bar"  | Message description
```

Successful Response: `HTTP 201 Created`
```json
{
    "message": "Message added"
}
```

***

**PUT /favorite/{id} | Ex: /favorite/4**

Modify an existing favorite message.
The user can only modify the favorites that the user owns.
Name and message cannot be empty. If empty string is given, it will be ignored.

Parameters:
```json
Optional:
    name: "FooBar"                  | Message title
    message: "https://www.foo.com/" | Favorite message
    description: "FooBar best Bar"  | Message description
```

Successful Response: `HTTP 200 OK`
```json
{
    "message": "Message updated"
}
```

Errors:
```json
HTTP 404 Not Found: Favorite message not found or does not belong to current user
```

***

**DELETE /favorite/{id} | Ex: /favorite/4**

Deletes favorite message with the specified id.
The user can only delete the favorites that the user owns.

Successful Response: `HTTP 200 OK`
```json
{
    "message": "Deleted if existed"
}
```

***
